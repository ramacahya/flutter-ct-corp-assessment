// import 'dart:async';
// import 'dart:convert';
// import 'package:mobx/mobx.dart';
// part 'movies.g.dart';
//
// class Movies = MoviesBase with _$Movies;
// abstract class MoviesBase with Store {
//   @observable
//   ObservableList movies = ObservableList.of([]);
//
//   @observable
//   double averageStars = 0;
//
//   @computed
//   int get numberOfReviews => movies.length;
//
//   @action
//   void addMovie(newMovie) {
//     movies.add(newMovie);
//   }
// }
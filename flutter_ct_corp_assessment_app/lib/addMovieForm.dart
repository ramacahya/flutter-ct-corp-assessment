import 'package:flutter/material.dart';

class AddMovieForm extends StatefulWidget {
  const AddMovieForm({Key? key}) : super(key: key);

  @override
  _AddMovieFormState createState() => _AddMovieFormState();
}

class _AddMovieFormState extends State<AddMovieForm> {

  final _formKey = GlobalKey();
  final _title = TextEditingController();
  final _director = TextEditingController();
  final _summary = TextEditingController();
  String _value = 'Adventure';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Movie'),
      ),
      body: Container(
        margin: EdgeInsets.only(top: 15),
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: Form(
          key: _formKey,
          child: Column(

            children: [
              TextFormField(
                controller: _title,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  hintText: 'Title',
                  labelText: 'Title',
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8)
                  )
                ),
              ),
              SizedBox(
                height: 5,
              ),
              TextFormField(
                controller: _director,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                    hintText: 'Director',
                    labelText: 'Director',
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)
                    )
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.black
                  ),
                  borderRadius: BorderRadius.circular(8)
                ),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton(
                    value: _value,
                    onChanged: (String ? newValue) {
                      setState(() {
                        _value = newValue!;
                      });
                    },
                    items: [
                      'Adventure', 'Action', 'Comedy', 'Fantasy', 'Horror', 'Sci-Fi',
                    ].map((e){
                      return DropdownMenuItem(child: Text(e), value: e,);
                    }).toList(),
                  ),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              TextFormField(
                controller: _summary,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                    hintText: 'Summary',
                    labelText: 'Summary',
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)
                    )
                ),
              ),
              SizedBox(
                height: 5,
              ),
              ElevatedButton(onPressed: () {}, child: Text('Submit'), style: ElevatedButton.styleFrom(fixedSize: Size(MediaQuery.of(context).size.width, 20)),)
            ],
          ),
        ),
      ),
    );
  }
}
